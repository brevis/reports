<?php

require __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

try
{
	(new Application('run', '1.0.0'))
		->register('run')
		->addArgument('input', InputArgument::REQUIRED, 'Input xlsx file')
		->addArgument('output_dir', InputArgument::REQUIRED, 'Output dir')
		->addArgument('sheets', InputArgument::REQUIRED, 'Sheets, example: 1-10')
		->addOption('open', null, InputOption::VALUE_OPTIONAL, 'Open output document after done')
		->setCode(function (InputInterface $input, OutputInterface $output) {
			$inputFileName  = $input->getArgument('input');
			$outputDir = $input->getArgument('output_dir');
			if (!is_file($inputFileName) || !is_readable($inputFileName)) {
				$output->writeln('<error>Input file not found or not readable</error>');
				return;
			}

			if (!is_writable(dirname($outputDir))) {
				$output->writeln('<error>Output file is not writable</error>');
				return;
			}

			$output->writeln('<info>Start process [' . basename($inputFileName) . ']...</info>');

			$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
			$in = $reader->load($inputFileName);
			$out = $reader->load(__DIR__ . '/tpl/out-new.xlsx');
			$outSheet = $out->getActiveSheet();

			$outColumns = array();
			$outCol = 7;
			while ($outCol <= 100) {
				$colName = strval($outSheet->getCellByColumnAndRow($outCol, 2));
				if ($colName == '') {
					break;
				}
				$outColumns[$colName] = $outCol;
				$outCol++;
			}

			$sheets = explode('-' ,$input->getArgument('sheets'));
			if (!isset($sheets[1])) {
				$sheets[1] = $sheets[0];
			}
			$sn = 1;
			$outStartRow = 4;
			for ($sheetName = $sheets[0]; $sheetName <= $sheets[1]; $sheetName++) {
				$output->write('<comment>Process sheet [' . $sheetName . ']...</comment>');

				$sheet = $in->getSheetByName(strval($sheetName));
				if ($sheet) {
					$row = $outStartRow + $sn - 1;

					// наименование
					$name = strval(trim($sheet->getCellByColumnAndRow(3, 6)));
					if (!$name || is_numeric($name) || $name == '(ТО, КР, ТР, реконструкция, монтаж, демонтаж)') {
						$name = strval($sheet->getCellByColumnAndRow(3, 7));
					}
					if (!$name || is_numeric($name) || $name == '(ТО, КР, ТР, реконструкция, монтаж, демонтаж)') {
						$name = strval(trim($sheet->getCellByColumnAndRow(3, 8)));
					}
					if (!$name || is_numeric($name) || $name == '(ТО, КР, ТР, реконструкция, монтаж, демонтаж)') {
						$name = strval(trim($sheet->getCellByColumnAndRow(3, 10)));
					}
					if (!$name || is_numeric($name) || $name == '(ТО, КР, ТР, реконструкция, монтаж, демонтаж)') {
						$name = strval(trim($sheet->getCellByColumnAndRow(1, 8)));
						$name = preg_replace('/Инвентарный номер и наименование:  /', '', $name);
					}
					$outSheet->setCellValueByColumnAndRow(1, $row, $sn);
					$outSheet->setCellValueByColumnAndRow(2, $row, $name);


					// Трудозатраты & Общая стоимость
					$total = '';
					$tr = '';
					$r = 10;
					while ($r <= 100) {
						$cellValue = mb_strtolower(trim(strval($sheet->getCellByColumnAndRow(2, $r))), 'UTF-8');
						if (($cellValue == 'всего' || $cellValue == 'всего:' || $cellValue == 'всего по акту:' || $cellValue == 'всего по акту')&& !$total) {
							$total = $sheet->getCell('N' . $r, false)->getCalculatedValue();
						}
						if (($cellValue == 'расчет заработной' || $cellValue == 'расчет заработной платы') && !$tr) {
							 $tr = $sheet->getCell('M' . $r, false)->getCalculatedValue();
						}
						$r++;
					}
					$outSheet->setCellValueByColumnAndRow(4, $row, round($tr, 2));
					$outSheet->setCellValueByColumnAndRow(6, $row, round($total, 2));

					// остальные данные
					$otherRow = 15;
					$otherColStart = 7;
					$allowedTypes = array('км', 'га', 'шт', 'дерево', 'м3', '100м', 'плита', 'шлейф', 'м');
					while (true && $otherRow <= 200) {
						$n = strval(trim($sheet->getCellByColumnAndRow(1, $otherRow)->getCalculatedValue()));
						$oName = strval(trim($sheet->getCellByColumnAndRow(2, $otherRow)->getCalculatedValue()));
						$cell = $sheet->getCell('K' . $otherRow, false);
						if ($cell) {
							$type = mb_strtolower(trim(strval($cell->getCalculatedValue())), 'UTF-8');
						} else {
							$type = '';
						}
						$cell = $sheet->getCell('L' . $otherRow, false);
						if ($cell) {
							$value = strval(trim($cell->getCalculatedValue()));
						} else {
							$value = '';
						}

						//echo $n . ' ' . $oName . ' ' . $type . ' ' . $value . PHP_EOL;

						if (!preg_match('/^\d+\.\d+$/', $n)
							&& !preg_match('/^\d+\.\d+\.$/', $n)
							&& !preg_match('/^\d+$/', $n)) {
							$otherRow++;
							continue;
						}

						if (in_array($type, $allowedTypes)) {

							// find col
							$foundCol = null;
							foreach ($outColumns as $on => $c) {
								$on = mb_strtolower($on, 'UTF-8');
								if (mb_strpos($on, mb_strtolower($oName, 'UTF-8'), 0, 'UTF-8') !== false) {
									$foundCol = $c;
									break;
								}
							}

							if (!$foundCol) {
								// or add new
								$foundCol = $otherColStart + count($outColumns);
								$outColumns[$oName] = $foundCol;
								$outSheet->setCellValueByColumnAndRow($foundCol, 2, $oName);
								$outSheet->getStyleByColumnAndRow($foundCol, 2)->getFill()
									->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
									->getStartColor()->setARGB('000FF000');
								$outSheet->setCellValueByColumnAndRow($foundCol, 3, $type);
							}

							$outSheet->setCellValueByColumnAndRow($foundCol, $row, $value);
						}

						$otherRow++;
					}

					$sn++;
				} else {
					$output->write('<error>sheet not found</error>');
				}

				$output->writeln('<comment>...done.</comment>');
			}

			$writer = new Xlsx($out);
			$outFileName = $outputDir . '/' . basename($inputFileName);
			$writer->save($outFileName);

			if ($input->getOption('open')) {
				shell_exec('open ' . $outFileName);
			}

			$output->writeln('<info>Done.</info>');

		})
		->getApplication()
		->setDefaultCommand('run', true)
		->run();
}
catch (Exception $e)
{
	echo 'Something went wrong' . PHP_EOL;
}